﻿using System;

namespace OopPrez
{
	class Program
	{
		static void Main(string[] args)
		{
			Animal animal = new Animal();
			animal.Name = "animal 1";
			animal.Sunet();


			Animal animal2 = new Animal("animal 2");
			Console.WriteLine(animal2.Name);
			animal2.Sunet();

			//Domestic domestic = new Domestic();
			//domestic.Name = "domestic 1";
			//domestic.Sunet();
			//domestic.Deplasare();
			//domestic.Deplasare(154);
			//domestic.Deplasare("alergare");
			//Console.Read();
		}
	}
	public class Animal
	{
		public Animal()
		{
			
		}

		public Animal(string Name)
		{
			this.Name = Name;
		}
		protected int NumberOfLegs { get; set; }

		public string Name { get; set; }

		public virtual void Sunet()
		{
			Name = "asd";
			Console.WriteLine("Sunet animal");
		}
	}

	public class Domestic : Animal
	{
		public Domestic()
		{
			Type = "first type";
		}

		private string Type { get; set; }

		public override void Sunet()
		{
			Console.WriteLine(Type);
			Console.WriteLine("Sunet Domestic");
		}

		public void Deplasare()
		{
			Console.WriteLine("Deplasare domestic");
		}

		public void Deplasare(int viteza)
		{
			Console.WriteLine("Deplasare domestic " + viteza);
		}

		public void Deplasare(string forma)
		{
			Console.WriteLine("Deplasare domestic " + forma);
		}
	}

	public class Salbatic : Animal
	{
		private string _gen;
		public string Gen { get; set; }

		public override void Sunet()
		{
			Console.WriteLine("Sunet Salbatic");
		}
	}

	public class Test : Domestic
	{
		public Test()
		{
		}
	}

}
