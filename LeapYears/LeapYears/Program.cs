﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeapYears
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                Console.WriteLine("Enter a year and find if it's a leap year:");

                int GivenYear = Convert.ToInt32(Console.ReadLine());
                if (GivenYear % 4 != 0)
                {
                    Console.WriteLine("It's a common year!");
                    Console.WriteLine(GivenYear);
                }
                else if (GivenYear % 100 != 0)
                {
                    Console.WriteLine("It's a leap year!");
                }
                else if (GivenYear % 400 != 0)
                {
                    Console.WriteLine("It's a common year!");
                }
                else
                    Console.WriteLine("It's a leap year!");
                    Console.ReadKey();
            }
        

        }
    }
} 

