﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace LastHomework
{
    class AgeChecker
    {
        static void Main(string[] args)
        {
            //Write a program that reads the list of people from a file and gives the users the option to check if a person is older than 18 or not. 
            //The result will be printed to the console(yes/ no, name of the person and age) and also exported to a file.
            //- the file with the data will be provided
            //- you must have 2 methods(1 for import, 1 for export)
            //- for search you must use LINQ
            //- for storing the data in memory you must use a generic collection

            Console.WriteLine("-=Age Checker=-");
            Console.WriteLine("-----");

            //1: import method:
            //====1. import file====

            Console.WriteLine("-----");
            Console.WriteLine("People in list: ");
            Console.WriteLine(" ");
            ImpList();
            ExpList();
        }
        public List<Person> ImpList()
        {
            string path = "people.txt";
            if (!File.Exists(path))
            {
                // file that will be imported
                using (StreamWriter sw = File.CreateText(path)) ;
                {
                    ;
                }
            }

            var lineCount = File.ReadLines(path).Count();
            Console.WriteLine("Number of lines in the imported file is: " + lineCount); //not required

            var lines = File.ReadLines(path);

            List<Person> peopleDict = new List<Person>();
            foreach (var line in lines)
            {
                string numbers = new String(line.Where(x => Char.IsDigit(x)).ToArray());
                int parsedNumber;
                bool isString = int.TryParse(numbers, out parsedNumber); //get numbers (ages) from line + convert 

                string letters = new String(line.Where(x => Char.IsLetter(x)).ToArray()); //get letters(names) from line

                //peopleDict.Add(letters, parsedNumber); //add names and ages to dictionary
                peopleDict.Add(new Person() { PersonName = letters, PersonAge = parsedNumber });
            }
            return peopleDict;
        }
        //2: export method:


        //List<string> exportList = new List<string>();
        public void ExpList()
        {
            string pathExport = "export.txt";
            if (!File.Exists(pathExport))
            {
                // file that will be exported
                using (StreamWriter sw = File.CreateText(pathExport)) ;
                {
                    ;
                }
            }
            using (StreamWriter sw = File.CreateText(pathExport))
            {

                foreach (var person in peopleDict)
                {
                    if (Person.PersonAge > 18)
                    {
                        Console.WriteLine("Name: {0}, Age: {1}, Over 18? Yes",
                            Person.PersonName, Person.PersonAge);
                        sw.WriteLine("Name: {0}, Age: {1}, Over 18? Yes",
                        item.Key, item.Value);

                    }
                    else if (item.Value > 0)
                    {
                        Console.WriteLine("Name: {0}, Age: {1}, Over 18? No",
                            item.Key, item.Value);
                        sw.WriteLine("Name: {0}, Age: {1}, Over 18? No",
                         item.Key, item.Value);
                    }
                    else
                    {
                        Console.WriteLine("Name: {0}, Age: n/a, Over 18? n/a ",
                            item.Key, item.Value);
                        sw.WriteLine("Name: {0}, Age: n/a, Over 18? n/a ",
                        item.Key, item.Value);
                    }
                }

            }

        }

        //Console.WriteLine("-----");

        //Console.WriteLine("Press a key to close the program ");
        //Console.ReadKey();
    

    public class Person : IEquatable<Person>
    {
        public string PersonName { get; set; }

        public int PersonAge { get; set; }

        public override string ToString()
        {
            return "Name: " + PersonName + "   Age: " + PersonAge;
        }
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            Person objAsPerson = obj as Person;
            if (objAsPerson == null) return false;
            else return Equals(objAsPerson);
        }
        public override int GetHashCode()
        {
            return PersonAge;
        }
        public bool Equals(Person other)
        {
            if (other == null) return false;
            return (this.PersonAge.Equals(other.PersonAge));
        }
            // Should also override == and != operators.
        }
    }
}