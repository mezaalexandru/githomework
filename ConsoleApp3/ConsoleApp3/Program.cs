﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            
}
        public class Person : IEquatable<Person>
        {
            public string PersonName { get; set; }

            public int PersonAge { get; set; }

            public override string ToString()
            {
                return "Name: " + PersonName + "   Age: " + PersonAge;
            }
            public override bool Equals(object obj)
            {
                if (obj == null) return false;
                Person objAsPerson = obj as Person;
                if (objAsPerson == null) return false;
                else return Equals(objAsPerson);
            }
            public override int GetHashCode()
            {
                return PersonAge;
            }
            public bool Equals(Person other)
            {
                if (other == null) return false;
                return (this.PersonAge.Equals(other.PersonAge));
            }
            // Should also override == and != operators.

        }
    }
}