﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace readwritesearch
{
    class Program
    {
        static void Main(string[] args)
        {
            //Import method
            //void Import()
            //{
                string[] lines = System.IO.File.ReadAllLines(@"C:\Test\People.txt");
            //}
            //Main function
            var lineCount = File.ReadLines(@"C:\Test\People.txt").Count();
            Console.WriteLine("Number of lines in the imported file is: " + lineCount); //not required
            List<string> peopleDict = new List<string>();
            foreach (var line in lines)
            {
                string numbers = new String(line.Where(x => Char.IsDigit(x)).ToArray());
                int parsedNumber;
                bool isString = int.TryParse(numbers, out parsedNumber); //get numbers (ages) from line + convert 

                string letters = new String(line.Where(x => Char.IsLetter(x)).ToArray()); //get letters(names) from line

                peopleDict.Add(letters, parsedNumber); //add names and ages to dictionary
            }

            Console.WriteLine("-----");
            Console.WriteLine("People in list: ");
            Console.WriteLine(" ");
        }


            
            using (StreamWriter sw = File.CreateText(pathExport))
            {

                foreach (KeyValuePair<string, int> item in peopleDict)
                {
                    if (item.Value > 18)
                    {
                        Console.WriteLine("Name: {0}, Age: {1}, Over 18? Yes",
                            item.Key, item.Value);
                        sw.WriteLine("Name: {0}, Age: {1}, Over 18? Yes",
                        item.Key, item.Value);

                    }
                    else if (item.Value > 0 && item.Value < 19)
                    {
                        Console.WriteLine("Name: {0}, Age: {1}, Over 18? No",
                            item.Key, item.Value);
                        sw.WriteLine("Name: {0}, Age: {1}, Over 18? No",
                         item.Key, item.Value);
                    }
                    else
                    {
                        Console.WriteLine("Name: {0}, Age: n/a, Over 18? n/a ",
                            item.Key, item.Value);
                        sw.WriteLine("Name: {0}, Age: n/a, Over 18? n/a ",
                        item.Key, item.Value);
                    }
                }
            };
        }
    }
}



